import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AngularMaterial2Component} from './angular-material2/angular-material2.component';

const routes: Routes = [
  {path: '', component: AngularMaterial2Component},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
