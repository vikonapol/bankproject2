import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from '@angular/forms';
import {MatSort, MatTableDataSource} from '@angular/material';
import {DataService} from './services/data.service';

export interface IRates {
  date: string;
  usd: number;
  eur: number;
  gbp: number;
}

@Component({
  selector: 'app-angular-material2',
  templateUrl: './angular-material2.component.html',
  styleUrls: ['./angular-material2.component.scss'],
})
export class AngularMaterial2Component implements OnInit {
  public date = new FormControl(new Date());
  public date2 = new FormControl(new Date());
  public displayedColumns: string[] = ['date', 'usd', 'eur', 'gbp'];
  public dataSource: MatTableDataSource<IRates>;
  private days = [];
  private myFormattedDate1: number;
  private myFormattedDate2: number;
  private correctorForSecondDate = 0;

  @ViewChild(MatSort) private sort: MatSort;

  constructor(private dataService: DataService) {  }

  public ngOnInit() {
    this.dataService.getRates('https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json')
      .subscribe((res) => {
      const oneDayRate = {
        date: res[0].exchangedate,
        usd: this.findCurrencyObj(res, 'USD').rate,
        eur: this.findCurrencyObj(res, 'EUR').rate,
        gbp: this.findCurrencyObj(res, 'GBP').rate,
      };
      this.dataSource = new MatTableDataSource([oneDayRate]);
    });
  }

  public findCurrencyObj(arr, currency) {
    return arr.find((item) => item.cc === currency);
  }

  public addDate(): void {
    this.myFormattedDate1 = +(this.date.value.toISOString().slice(0, 10).replace(/[-]/g, '')) + 1;
    this.myFormattedDate2 = +(this.date2.value.toISOString()
      .slice(0, 10).replace(/[-]/g, '')) + this.correctorForSecondDate;

    const difference = this.myFormattedDate2 - this.myFormattedDate1;
    if (difference > 5 || difference === 5 ) {
      alert('Введите диапазон меньше');
    } else {
      this.days.push(this.myFormattedDate1);
      let tempDate = this.myFormattedDate1;
      for (let i = 1; i < difference; i++) {
        tempDate += 1;
        this.days.push(tempDate);
      }
      this.days.push(this.myFormattedDate2);
    }
    if (this.days) {
      this.days.forEach((day) => {
        this.dataService.getRates(`https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?date=${day}&json`)
          .subscribe((res) => {
          const currentDayRate = {
            date: res[0].exchangedate,
            usd: this.findCurrencyObj(res, 'USD').rate,
            eur: this.findCurrencyObj(res, 'EUR').rate,
            gbp: this.findCurrencyObj(res, 'GBP').rate,
          };
          this.dataSource.data.push(currentDayRate);
          this.dataSource.sort = this.sort;
        });

      });
    }
    this.clear();
  }

  public clear() {
    this.days = [];
    this.dataSource.data = [];
    this.correctorForSecondDate = 1;
  }
}
