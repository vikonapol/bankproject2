import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AngularMaterial2Component} from './angular-material2.component';

const purchaseRoutes: Routes = [
  {path: '', component: AngularMaterial2Component},
];

@NgModule({
  imports: [RouterModule.forChild(purchaseRoutes)],
  exports: [RouterModule],
})
export class AngularMaterial2Routing { }
